FROM alpine

# https://github.com/jedisct1/dnscrypt-proxy

RUN apk add --no-cache --update --repository https://dl-cdn.alpinelinux.org/alpine/edge/community \
        dnscrypt-proxy \
        ca-certificates \
    && \
    rm -rf /var/cache/apk/*
    
ADD config /config

EXPOSE 53/udp

CMD ["dnscrypt-proxy", "-config", "/config/dnscrypt-proxy.toml"]
